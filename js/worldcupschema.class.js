function WorldCupSchema(groups, matches) {
    this.groups = groups;
    this.matches = matches;

    var tblGroupStage = document.getElementById('tblGroupStage'),
        tblRoundOf16 = document.getElementById('tblRoundOf16'),
        tblQuarterFinals = document.getElementById('tblQuarterFinals'),
        tblSemiFinals = document.getElementById('tblSemiFinals'),
        tblBronzeFinal = document.getElementById('tblBronzeFinal'),
        tblFinal = document.getElementById('tblFinal');

    for (var i = 0; i < this.matches.length; i++) {
        var that = this.matches[i];

        if (that.group === "R2") {
            tblRoundOf16.appendChild(that.row);
        } else if (that.group === "QTR") {
            tblQuarterFinals.appendChild(that.row);
        } else if (that.group === "SF") {
            tblSemiFinals.appendChild(that.row);
        } else if (that.group === "BRONZE") {
            tblBronzeFinal.appendChild(that.row);
        } else if (that.group === "FINAL") {
            tblFinal.appendChild(that.row);
        } else {
            tblGroupStage.appendChild(that.row);
        }
    }
}

WorldCupSchema.prototype.updateAll = function () {
    var rows = document.querySelectorAll('tr[class="unknown"]');

    for (var i = 0; i < rows.length; i++) {
        var cells = rows[i].querySelectorAll('td'),
            t1 = cells[3].getAttribute('data'),
            t2 = cells[7].getAttribute('data'),
            home_group = t1[0].toUpperCase(),
            home_place = t1[1],
            away_group = t2[0].toUpperCase(),
            away_place = t2[1];

        cells[3].innerText = this.groups[home_group].getTeamByPlace(home_place);
        cells[7].innerText = this.groups[away_group].getTeamByPlace(away_place);
    }
};

WorldCupSchema.prototype.updatePoints = function () {
    for (var i = 0; i < this.matches.length; i++) {
        var that = this.matches[i];

        // Matches after 56 will not be determined by group position
        if (that.group.length === 1) {
            if (that.getWinner() === -1) {
                console.log(that.group);
                this.groups[that.group].teams[that.team1.name] += 1;
                this.groups[that.group].teams[that.team2.name] += 1;
            } else {
                this.groups[that.group.toUpperCase()].teams[that.getWinner()] += 3;
            }
        }

    }
};

WorldCupSchema.prototype.updateMatches = function () {

    // TODO
    //
    // 1. Reset Group score before updating
    // Update scores round by round. Count Group stage, then RO16, QTR etc.

    this.matches.forEach(function (item) {
        item.setScore();
    });

    this.updatePoints();

    for (var i = 0; i < this.matches.length; i++) {
        var that = this.matches[i];

        var t1 = that.team1.name.split(''),
            t2 = that.team2.name.split('');
        
        // Update teams on 'Round of 16'
        if (that.id < 57 && that.id > 48) {

            var home_group = t1[0].toUpperCase(),
                home_place = t1[1],
                home_name = this.groups[home_group].getTeamByPlace(home_place),
                away_group = t2[0].toUpperCase(),
                away_place = t2[1],
                away_name = this.groups[away_group].getTeamByPlace(away_place);

                that.homeCell.innerText = home_name;
                that.awayCell.innerText = away_name;
                
        }

        // Update teams on QTR, SF, BRONZE, FINAL
        if (that.id > 56) {
            var home_candidate = t1[0],
                home_match_id = t1.splice(1),
                away_candidate = t2[0],
                away_match_id = t2.splice(1),
                home_name = "",
                away_name = "";
            
            for (var j = 0; j < this.matches.length; j++) {
                var thatm = this.matches[j];

                if (thatm.id === home_match_id && home_candidate === "w") {
                    home_name = thatm.getWinner();
                } else if (thatm.id === away_match_id && away_candidate === "w") {
                    away_name = thatm.getWinner();
                }
            }

            that.homeCell.innerText = home_name;
            that.awayCell.innerText = away_name;
        }
    }
};