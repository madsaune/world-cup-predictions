// RO16
// C1,D2
// A1,B2
// B1,A2
// D1,C2
// E1,F2
// G1,H2
// F1,E2
// H1,G2

// QTR
// W49,W50
// W53,W54
// W55,W56
// W51,W52

// SF
// W57,W58
// W59,W60

// 3rd
// L61,L62

// FINAL
// W62,W61

var groups = {
    "A" : new Group({
        "Russia": 4,
        "Egypt": 6,
        "Uruguay": 2,
        "Saudi Arabia": 1
    }),
    "B" : new Group({
        "Portugal": 5,
        "Spain": 3,
        "Marocco": 2,
        "Iran": 0
    }),
    "C" : new Group({
        "France": 7,
        "Australia": 5,
        "Peru": 3,
        "Denmark": 1
    }),
    "D" : new Group({
        "Argentina": 8,
        "Iceland": 3,
        "Croatia": 1,
        "Nigeria": 5
    }),
    "E" : new Group({
        "Brazil": 0,
        "Switzerland": 0,
        "Costa Rica": 0,
        "Serbia": 0
    }),
    "F" : new Group({
        "Germany": 0,
        "Mexico": 0,
        "Sweden": 0,
        "South Korea":0
    }),
    "G" : new Group({
        "Belgium": 0,
        "Panama":0 ,
        "Tunisia": 0,
        "England":0 
    }),
    "H" : new Group({
        "Poland": 0,
        "Senegal": 0,
        "Colombia": 0,
        "Japan": 0
    })
};

var teams = [
    new Team("Russia", "A"),
    new Team("Saudi Arabia", "A"),
    new Team("Egypt", "A"),
    new Team("Uruguay", "A"),
    new Team("Portugal", "B"),
    new Team("Spain", "B"),
    new Team("Morocco", "B"),
    new Team("Iran", "B"),
    new Team("France", "C"),
    new Team("Australia", "C"),
    new Team("Peru", "C"),
    new Team("Denmark", "C"),
    new Team("Argentina", "D"),
    new Team("Iceland", "D"),
    new Team("Croatia", "D"),
    new Team("Nigeria", "D"),
    new Team("Brazil", "E"),
    new Team("Switzerland", "E"),
    new Team("Costa Rica", "E"),
    new Team("Serbia", "E"),
    new Team("Germany", "F"),
    new Team("Mexico", "F"),
    new Team("Sweden", "F"),
    new Team("South Korea", "F"),
    new Team("Belgium", "G"),
    new Team("Panama", "G"),
    new Team("Tunisia", "G"),
    new Team("England", "G"),
    new Team("Poland", "H"),
    new Team("Senegal", "H"),
    new Team("Colombia", "H"),
    new Team("Japan", "H")
];

var matches = [
    new Match("A", 1, "Russia", "Saudi Arabia", "14. Juni 2018 17:00", "Moscow"),
    new Match("A", 2, "Egypt", "Uruguay", "15. Juni 2018 14:00", "Saint Petersburg"),
    new Match("B", 4, "Marocco", "Iran", "15. Juni 2018 17:00", "Saint Petersburg"),
    new Match("B", 3, "Portugal", "Spain", "15. Juni 2018 20:00", "Sochi"),
    new Match("C", 5, "France", "Australia", "16. Juni 2018 12:00", "Kazan"),
    new Match("D", 7, "Argentina", "Iceland", "16. Juni 2018 15:00", "Moscow"),
    new Match("C", 6, "Peru", "Denmark", "16. Juni 2018 18:00", "Saransk"),
    new Match("D", 8, "Croatia", "Nigeria", "16. Juni 2018 21:00", "Kaliningrad"),
    new Match("E", 10, "Costa Rica", "Serbia", "17. Juni 2018 14:00", "Samara"),
    new Match("F", 11, "Germany", "Mexico", "17. Juni 2018 17:00", "Moscow"),
    new Match("E", 9, "Brazil", "Switzerland", "17. Juni 2018 20:00", "Rostov-on-Don"),
    new Match("F", 12, "Sweden", "South Korea", "18. Juni 2018 14:00", "Nizhny Novgorod"),
    new Match("G", 13, "Belgium", "Panama", "18. Juni 2018 17:00", "Sochi"),
    new Match("G", 14, "Tunisia", "England", "18. Juni 2018 20:00", "Volgograd"),
    new Match("H", 16, "Colombia", "Japan", "19. Juni 2018 14:00", "Saransk"),
    new Match("H", 15, "Poland", "Senegal", "19. Juni 2018 17:00", "Moscow"),
    new Match("A", 17, "Russia", "Egypt", "19. Juni 2018 20:00", "Saint Petersburg"),
    new Match("B", 19, "Portugal", "Morocco", "20. Juni 2018 14:00", "Moscow"),
    new Match("A", 18, "Uruguay", "Saudi Arabia", "20. Juni 2018 17:00", "Rostov-on-Don"),
    new Match("B", 20, "Iran", "Spain", "20. Juni 2018 20:00", "Kazan"),
    new Match("C", 22, "Denmark", "Australia", "21. Juni 2018 14:00", "Samara"),
    new Match("C", 21, "France", "Peru", "21. Juni 2018 17:00", "Yekaterinburg"),
    new Match("D", 23, "Argentina", "Croatia", "21. Juni 2018 20:00", "Nizhny Novgorod"),
    new Match("E", 25, "Brazil", "Costa Rica", "22. Juni 2018 14:00", "Saint Petersburg"),
    new Match("D", 24, "Nigeria", "Iceland", "22. Juni 2018 17:00", "Volgograd"),
    new Match("E", 26, "Serbia", "Switzerland", "22. Juni 2018 20:00", "Kaliningrad"),
    new Match("G", 29, "Belgium", "Tunisia", "23. Juni 2018 14:00", "Moscow"),
    new Match("F", 28, "South Korea", "Mexico", "23. Juni 2018 17:00", "Rostov-on-Don"),
    new Match("F", 27, "Germany", "Sweden", "23. Juni 2018 20:00", "Sochi"),
    new Match("G", 30, "England", "Panama", "24. Juni 2018 14:00", "Nizhny Novgorod"),
    new Match("H", 32, "Japan", "Senegal", "24. Juni 2018 17:00", "Yekaterinburg"),
    new Match("H", 31, "Poland", "Colombia", "24. Juni 2018 20:00", "Kazan"),
    new Match("A", 33, "Uruguay", "Russia", "25. Juni 2018 16:00", "Samara"),
    new Match("A", 34, "Saudi Arabia", "Egypt", "25. Juni 2018 16:00", "Volgograd"),
    new Match("B", 35, "Iran", "Portugal", "25. Juni 2018 20:00", "Saransk"),
    new Match("B", 36, "Spain", "Morocco", "25. Juni 2018 20:00", "Kaliningrad"),
    new Match("C", 37, "Denmark", "France", "26. Juni 2018 16:00", "Moscow"),
    new Match("C", 38, "Australia", "Peru", "26. Juni 2018 16:00", "Sochi"),
    new Match("D", 39, "Nigeria", "Argentina", "26. Juni 2018 20:00", "Saint Petersburg"),
    new Match("D", 40, "Iceland", "Croatia", "26. Juni 2018 20:00", "Rostov-on-Don"),
    new Match("F", 43, "South Korea", "Germany", "27. Juni 2018 16:00", "Kazan"),
    new Match("F", 44, "Mexico", "Sweden", "27. Juni 2018 16:00", "Yekaterinburg"),
    new Match("E", 41, "Serbia", "Brazil", "27. Juni 2018 20:00", "Moscow"),
    new Match("E", 42, "Switzerland", "Costa Rica", "27. Juni 2018 20:00", "Nizhny Novgorod"),
    new Match("H", 47, "Japan", "Poland", "28. Juni 2018 16:00", "Volgograd"),
    new Match("H", 48, "Senegal", "Colombia", "28. Juni 2018 16:00", "Samara"),
    new Match("G", 45, "England", "Belgium", "28. Juni 2018 20:00", "Kaliningrad"),
    new Match("G", 46, "Panama", "Tunisia", "28. Juni 2018 20:00", "Saransk"),

    // ROUND 2
    new Match("R2", 50, "c1", "d2", "30. Juni 2018 16:00", "Kazan"),
    new Match("R2", 49, "a1", "b2", "30. Juni 2018 20:00", "Sochi"),
    new Match("R2", 51, "b1", "a2", "01. Juli 2018 16:00", "Moscow"),
    new Match("R2", 52, "d1", "c2", "01. Juli 2018 20:00", "Nizhny Novgorod"),
    new Match("R2", 53, "e1", "f2", "02. Juli 2018 16:00", "Samara"),
    new Match("R2", 54, "g1", "h2", "02. Juli 2018 20:00", "Rostov-on-Don"),
    new Match("R2", 55, "f1", "e2", "03. Juli 2018 16:00", "Saint Petersburg"),
    new Match("R2", 56, "h1", "g2", "03. Juli 2018 20:00", "Moscow"),

    // QTR
    new Match("QTR", 57, "w49", "w50", "06. Juli 2018 16:00", "Nizhny Novgorod"),
    new Match("QTR", 58, "w53", "w54", "06. Juli 2018 20:00", "Nizhny Novgorod"),
    new Match("QTR", 60, "w55", "w56", "07. Juli 2018 16:00", "Nizhny Novgorod"),
    new Match("QTR", 59, "w51", "w52", "07. Juli 2018 20:00", "Nizhny Novgorod"),

    // SF
    new Match("SF", 61, "w57", "w58", "10. Juli 2018 20:00", "Saint Petersburg"),
    new Match("SF", 62, "w59", "w60", "11. Juli 2018 20:00", "Moscow"),

    // BRONZE
    new Match("BRONZE", 63, "l61", "l62", "14. Juli 2018 16:00", "Saint Petersburg"),

    // FINAL
    new Match("FINAL", 64, "w62", "w61", "15. Juli 2018 17:00", "Moscow")
];

var WCS = new WorldCupSchema(groups, matches);

