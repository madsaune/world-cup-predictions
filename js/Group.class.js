function Group(name) {
    this.name = name;
    this.teams = [];
}

Group.prototype.addTeam = function (team) {
    this.teams.push(team);
};

Group.prototype.getTeamByPlace = function (place) {
    this.sort();
    return this.teams[place-1];
};

Group.prototype.sort = function () {
    var swapped;
    
    do {
        swapped = false;
        for (var i=0; i < this.teams.length-1; i++) {
            if (this.teams[i].points > this.teams[i+1].points) {
                var temp = this.teams[i];
                this.teams[i] = this.teams[i+1];
                this.teams[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);

    // return Object.entries(this.teams).sort(function (a, b) {
    //     return a[1] - b[1];
    // }).reverse();
};