function Match(group, id, team1, team2, date, stadium) {
    if (arguments.length > 1) {
        this.group = group;
        this.id = id;
        this.team1 = team1;
        this.team2 = team2;
        this.date = date;
        this.stadium = stadium;
        
        this.makeHTML();
    } else {
        this.group = group.group;
        this.id = group.id;
        this.team1 = group.team1;
        this.team2 = group.team2;
        this.date = group.date;
        this.stadium = group.stadium;
    }
}

Match.prototype.setScore = function () {
    this.team1.score = this.homeScoreCellInput.value;
    this.team2.score = this.awayScoreCellInput.value;
};

Match.prototype.getWinner = function () {
    if (this.team1.score > this.team2.score) {
        return this.team1.name;
    } else if (this.team1.score < this.team2.score) {
        return this.team2.name;
    } else {
        return -1;
    }
};

Match.prototype.getLooser = function () {
    if (this.team1.score < this.team2.score) {
        return this.team1.name;
    } else if (this.team1.score > this.team2.score) {
        return this.team2.name;
    } else {
        return -1;
    }
};

Match.prototype.updateRow = function () {
    this.homeCell.innerText = this.team1.name;
    this.homeScoreCellInput.value = this.team1.score;
    this.awayCell.innerText = this.team2.name;
    this.awayScoreCellInput.value = this.team2.score;
};

Match.prototype.makeHTML = function () {    
    var idCellText = document.createTextNode(this.id),
        dateCellText = document.createTextNode(this.date),
        homeCellText = document.createTextNode(this.team1.name),
        dividerCellText = document.createTextNode("-"),
        awayCellText = document.createTextNode(this.team2.name),
        stadiumCellText = document.createTextNode(this.stadium);
    
    this.row = document.createElement('tr');
    this.row.setAttribute('id', "match-"+this.id);

    this.idCell = document.createElement('td');
    this.idCell.setAttribute('class', 'id');

    this.dateCell = document.createElement('td');
    this.dateCell.setAttribute('class', 'date');
    
    this.homeCell = document.createElement('td');
    this.homeCell.setAttribute('class', 'home-name');

    this.homeScoreCell = document.createElement('td');
    this.homeScoreCellInput = document.createElement('input');
    this.homeScoreCellInput.setAttribute('type', 'text');

    this.dividerCell = document.createElement('td');
    this.dividerCell.setAttribute('class', 'divider');

    this.awayScoreCell = document.createElement('td');
    this.awayScoreCellInput = document.createElement('input');
    this.awayScoreCellInput.setAttribute('type', 'text');

    this.awayCell = document.createElement('td');
    this.awayCell.setAttribute('class', 'away-name');

    this.stadiumCell = document.createElement('td');
    this.stadiumCell.setAttribute('class', 'stadium');
    
    this.homeScoreCellInput.value = this.team1.score;
    this.awayScoreCellInput.value = this.team2.score;
    this.idCell.appendChild(idCellText);
    this.dateCell.appendChild(dateCellText);
    this.homeCell.appendChild(homeCellText);
    this.homeScoreCell.appendChild(this.homeScoreCellInput);
    this.dividerCell.appendChild(dividerCellText);
    this.awayScoreCell.appendChild(this.awayScoreCellInput);
    this.awayCell.appendChild(awayCellText);
    this.stadiumCell.appendChild(stadiumCellText);

    if (!this.group.match(/^[A-H]$/g)) {
        this.homeCell.setAttribute('data', this.team1.name);
        this.awayCell.setAttribute('data', this.team2.name);
    }
    
    this.row.appendChild(this.idCell);
    this.row.appendChild(this.dateCell);
    this.row.appendChild(this.homeCell);
    this.row.appendChild(this.homeScoreCell);
    this.row.appendChild(this.dividerCell);
    this.row.appendChild(this.awayScoreCell);
    this.row.appendChild(this.awayCell);
    this.row.appendChild(this.stadiumCell);
    
    return this.row;
};