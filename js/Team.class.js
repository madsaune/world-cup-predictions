function Team(name, group) {
    this.name = name;
    this.group = group;
    this.points = 0;
}

Team.prototype.addPoints = function (points) {
    if (isNumeric(points)) {
        this.points += points;
    }
};

Team.prototype.subPoints = function (points) {
    if (isNumeric(points)) {
        this.points -= points;
    }
};